%initializes default figure and plotting properties
%call from startup.m

% set some other default values
set(0, 'RecursionLimit', 50);
set(0, 'DefaultFigurePaperType', 'USletter');
set(0, 'DefaultFigureWindowStyle', 'normal');
set(0, 'DefaultAxesBox', 'on');

%set default font sizes
set(0, 'DefaultTextFontSize', 26);
set(0, 'DefaultAxesFontSize', 26);
set(0, 'DefaultUicontrolFontSize', 8);

set(0, 'DefaultLineLineWidth',3)

set(0,'DefaultFigurePosition',[200 200 1024 768]')

